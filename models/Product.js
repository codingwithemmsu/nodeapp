const products = [
    {
      "colors": ["Blue", "White", "Black"],
      "_id": "109pb5b75607497b98882bda5b504926",
      "name": "Iphone 8 256G",
      "price": 1849,
      "imageUrl": "iphone8.avif",
      "description": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id laborum.",
    },
    {
      "colors": ["Black/Yellow", "Black/Red"],
      "_id": "90892cacb65d43b2b5c1ff70f3393ad1",
      "name": "Iphone 11 128G",
      "price": 4499,
      "imageUrl": "iphone11.jpeg",
      "description": "Morbi nec erat aliquam, sagittis urna non, laoreet justo. Etiam sit amet interdum diam, at accumsan lectus.",
    },
    {
      "colors": ["Green", "Red", "Orange"],
      "_id": "098273915a544fde83cfdfc904935ee7",
      "name": "Iphoe 13  ",
      "price": 7199,
      "imageUrl": "iphone13.png",
      "description": "Pellentesque fermentum arcu venenatis ex sagittis accumsan. Vivamus lacinia fermentum tortor.Mauris imperdiet dt.",
    },
    {
      "colors": ["Green", "Red", "Orange"],
      "_id": "098273915a544fde83cfdfc904935ee7",
      "name": "Iphoe 7  ",
      "price": 2199,
      "imageUrl": "iphone7.png",
      "description": "Pellentesque fermentum arcu venenatis ex sagittis accumsan. Vivamus lacinia fermentum tortor.Mauris imperdiet imperdiet imperdiet imperdiet.",
    }
  ];
  
  exports.find = () => {
    return new Promise((resolve, reject) => resolve(JSON.parse(JSON.stringify(products))));
  }
  
  exports.findById = (id) => {
    return new Promise((resolve, reject) =>
      resolve(JSON.parse(JSON.stringify(products)).find(product =>
        product._id == id)
      )
    );
  }
  
  
  
  